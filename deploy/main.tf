terraform {
  backend "s3" {
    bucket         = "recipe-app-api-application" // s3 bucket
    key            = "recipe-app.tfstate"
    region         = "eu-central-1"
    encrypt        = true
    dynamodb_table = "smartmove-app-api-tf-state-lock" // for blocking another work on terraform
  }
}

provider "aws" {
  region  = "eu-central-1"
  version = "~> 2.54.0" // aws terraform verrsion
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
}